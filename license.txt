LICENSE CERTIFICATE : Envato Market Item
==============================================

This document certifies the purchase of:
ONE REGULAR LICENSE
as defined in the standard terms and conditions on Envato Market.

Licensor's Author Username:
bessemzitouni

Licensee:
Daniel Rocha Ramirez

Item Title:
Smart Invoice System

Item URL:
https://codecanyon.net/item/smart-invoice-system/20321868

Item ID:
20321868

Item Purchase Code:
257ee5ce-80f4-4499-93ce-740d8bfabfc6

Purchase Date:
2020-06-18 03:28:36 UTC

For any queries related to this document or license please contact Help Team via https://help.author.envato.com

Envato Pty. Ltd. (ABN 11 119 159 741)
PO Box 16122, Collins Street West, VIC 8007, Australia

==== THIS IS NOT A TAX RECEIPT OR INVOICE ====

